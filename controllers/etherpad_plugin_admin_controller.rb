class EtherpadPluginAdminController < AdminController
  append_view_path File.join(File.dirname(__FILE__) + '/../views')

  def index
    @settings ||= Noosfero::Plugin::Settings.new(environment, EtherpadPlugin, params[:settings])
    if request.post?
      begin
        settings.save!
        session[:notice] = _('Settings saved!')
        redirect_to :controller => 'plugins', :action => 'index'
      rescue
        session[:notice] = _('Settings could not be saved...')
      end
    end
  end

  private

  def settings
    @settings ||= Noosfero::Plugin::Settings.new(environment, EtherpadPlugin)
  end
end
