class EtherpadPlugin < Noosfero::Plugin

  def self.plugin_name
    "Etherpad"
  end

  def self.plugin_description
    _("Adds the pad as a new kind of content.")
  end

  def js_files
    'etherpad-lite-jquery-plugin/js/etherpad.js'
  end

  def content_types
    EtherpadPlugin::Pad
  end

  def stylesheet?
    true
  end

end
