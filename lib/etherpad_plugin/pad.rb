class EtherpadPlugin::Pad < Article
  settings_items :etherpad_plugin_id, :type => 'String'
  settings_items :etherpad_plugin_server, :type => 'String'
  attr_accessible :etherpad_plugin_id, :etherpad_plugin_server

  validates_presence_of :etherpad_plugin_server
  validates_presence_of :etherpad_plugin_id

  SEPARATOR='/p/'

  before_validation do |pad|
    pad.etherpad_plugin_server = etherpad_plugin_settings.url if pad.etherpad_plugin_server.blank?
    pad.etherpad_plugin_id = SecureRandom.hex if pad.etherpad_plugin_id.blank?
  end

  # Ensure pad is created, if connected via API.
  after_create do |pad|
    begin
      pad.etherpad_plugin_connection.pad(etherpad_plugin_id)
    rescue RestClient::Unauthorized
      # Just chill. Unable to create pad because there's no API key for it.
    end
  end

  def self.icon_name(article = nil)
    'pad'
  end

  def self.short_description
    _('Pad')
  end

  def self.description
    _('Creates a colaborative pad.')
  end

  def self.edits_lead_and_body?
    false
  end

  def accept_comments?
    true
  end

  def to_html(options = {})
    lambda do
      render :file => 'content_viewer/pad.html.erb'
    end
  end

  def etherpad_plugin_url
    etherpad_plugin_server + SEPARATOR + etherpad_plugin_id
  end

  def etherpad_plugin_settings
    @settings ||= Noosfero::Plugin::Settings.new(profile.environment, EtherpadPlugin)
  end

  def etherpad_plugin_connection
    @connection ||= EtherpadLite.connect(etherpad_plugin_settings.url, etherpad_plugin_settings.api_key)
    @connection
  end
end
